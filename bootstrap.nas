# Our current namespace becomes the global namespace. This is for
# backwards-compatibility. According to Andy Ross:
#
#   Add it to itself as a recursive sub-reference under the name
#   "globals". This gives client-code write access to the
#   namespace if someone wants to do something fancy.
#   ($FG_SRC/Scripting/NasalSys.cxx, lines 716-620, 2012)
var globals = caller(0)[0];

# Load a sub-module or file if not already loaded.
# @param module_path A sufficiently resolvable path pointing
#                    to a *.nas file or a directory of *.nas
#                    files and/or subdirectories.
# @param symbols A list of symbols, as per _require_symbols(),
#                to require the module to have once loaded or,
#                in the case of a circular dependency, once
#                recursively required again. An empty list just
#                requires that the whole file is loaded before
#                continuing. Nil says "I don't care".
var require = func(module_path, symbols=nil) {
	find_resource(module_path, var resources = []);
	foreach (var resource; resources) {
		if (resource.getValue("loaded")) {
			if (symbols == nil) continue;
			var namespace = globals[var ns = resource.getValue("namespace")];
			var sym = _require_symbols(namespace, symbol);
			if (sym != nil)
				die(sprintf("symbolic dependency failed: symbol '%s' not"
				            " found in module '%s' after loading", sym, ns));
			continue;
		} elsif (resource.getValue("load-requested")) {
			if (symbols == nil) continue;
			var namespace = globals[var ns = resource.getValue("namespace")];
			if (typeof(symbols) == 'scalar')
				symbols = [symbols];
			elsif (!size(symbols))
				die("circular dependency failed: resource wasn't loaded");
			var sym = _require_symbols(namespace, symbol);
			if (sym != nil)
				die(sprintf("circular dependency failed: symbol '%s' not"
				            " found in module '%s'", sym, ns));
			continue;
		} else
		resource.setValue("load-requested", 1);
		# listeners fire
		if (!resource.getValue("loaded")) die();
		if (symbols == nil) continue;
		var namespace = globals[var ns = resource.getValue("namespace")];
		var sym = _require_symbols(namespace, symbols);
		if (sym != nil)
			die(sprintf("symbolic dependency failed: symbol '%s' not"
			            " found in module '%s' after loading", sym, ns));
	}
	return resources;
}

# Private; for each symbol in symbols, return it if it doesn't
# exist. Each symbol can be a scalar (first-level symbol) or
# a list of scalars (multi-level symbol, e.g. props.Node.getValues).
var _require_symbols = func(namespace, symbols) {
	if (typeof(symbols) == 'scalar')
		symbols = [symbols];
	foreach (var sym; symbols) {
		if (typeof(sym == 'scalar')) {
			if (!contains(namespace, sym))
				return sym;
		} else {
			var current = namespace;
			var ret = "";
			foreach (var subsym; sym) {
				if (!contains(current, subsym))
					return ret~subsym;
				else {
					current = current[subsym];
					ret ~= subsym~".";
				}
			}
		}
	}
	return nil;
}

###########@@@@##############@@@#@@########
## VERY UGLY DEPENDENCY RESOLUTION!! BEWARE
###########@@@@##############@@@#@@########



if (!contains(globals, "io")) var io = {};
io.load_nasal = bind(func(file, module=nil) {
	# Loads Nasal file into namespace and executes it. The namespace
	# (module name) is taken from the optional second argument, or
	# derived from the Nasal file's name.
	#
	# Usage:   io.load_nasal(<filename> [, <modulename>]);
	#
	# Example:
	#
	#     io.load_nasal(getprop("/sim/fg-root") ~ "/Local/test.nas");
	#     io.load_nasal("/tmp/foo.nas", "test");
	#
	if (module == nil)
		module = split(".", split("/", file)[-1])[0];

	printlog("info", "loading ", file, " into namespace ", module);

	if (!contains(globals, module))
		globals[module] = {};
	elsif (typeof(globals[module]) != "hash")
		die("io.load_nasal(): namespace '" ~ module ~ "' already in use, but not a hash");

	var code = call(func compile(readfile(file), file), nil, var err = []);
	if (size(err)) {
		if (substr(err[0], 0, 12) == "Parse error:") { # hack around Nasal feature
		    var e = split(" at line ", err[0]);
		    if (size(e) == 2)
		        err[0] = string.join("", [e[0], "\n  at ", file, ", line ", e[1], "\n "]);
		}
		for (var i = 1; (var c = caller(i)) != nil; i += 1)
		    err ~= subvec(c, 2, 2);
		printerror(err);
		return 0;
	}
	call(bind(code, globals), nil, nil, globals[module], err);
	if (size(err)) printerror(err);
	return !size(err);
}, io, require);
io.readfile = bind(func(file) {
	# Reads and returns a complete file as a string
	if ((var st = io.stat(file)) == nil)
		die("Cannot stat file: " ~ file);
	var sz = st[7];
	var buf = bits.buf(sz);
	read(open(file), buf, sz);
	return buf;
}, io, require);
io.printerror = func(err) {
	if (!size(err))
		return;
	print(sprintf("%s:\n at %s, line %d", err[0], err[1], err[2]));
	for (var i = 3; i < size(err); i += 2)
		print(sprintf("  called from: %s, line %d", err[i], err[i + 1]));
};

io.load_nasal(getprop("/sim/fg-root")~"/Nasal/props.nas");

##
# Wrapper for the _setlistener function. Takes a property path string
# or props.Node object in arg[0] indicating the listened to property,
# a function in arg[1], an optional bool in arg[2], which triggers the
# function initially if true, and an optional integer in arg[3], which
# sets the listener's runtime behavior to "only trigger on change" (0),
# "always trigger on write" (1), and "trigger even when children are
# written to" (2).
#
var setlistener = func(node, fn, init = 0, runtime = 1) {
    if(node.parents[0] == props.Node) node = node._g;
    elsif(typeof(node) != "scalar" and typeof(node) != "ghost")
        die("bad argument to setlistener()");
    var id = _setlistener(node, func(chg, lst, mode, is_child) {
        fn(props.wrapNode(chg), props.wrapNode(lst), mode, is_child);
    }, init, runtime);
    var c = caller(1);
    logprint(1, sprintf("setting listener #%d in %s, line %s", id, c[2], c[3]));
    return id;
}

##
# Join all elements of a list inserting a separator between every two of them.
#
join = func(sep, list) {
	if (!size(list))
		return "";
	var str = list[0];
	foreach (var s; subvec(list, 1))
		str ~= sep ~ s;
	return str;
}


##
# Replace all occurrences of 'old' by 'new'.
#
replace = func(str, old, new) {
	return join(new, split(old, str));
}

##
# Removes superfluous slashes, empty and "." elements, expands
# all ".." elements, and turns all backslashes into slashes.
# The result will start with a slash if it started with a slash
# or backslash, it will end without slash. Should be applied to
# absolute property or file paths, otherwise ".." elements might
# be resolved wrongly.
#
normpath = func(path) {
	path = replace(path, "\\", "/");
	var prefix = size(path) and path[0] == `/` ? "/" : "";
	var stack = [];

	foreach (var e; split("/", path)) {
		if (e == "." or e == "")
			continue;
		elsif (e == "..")
			pop(stack);
		else
			append(stack, e);
	}
	return size(stack) ? prefix ~ join("/", stack) : "/";
}

###########@@@@##############@@@#@@########
## END UGLY DEPENDENCY RESOLUTION!!! Yay!!!
###########@@@@##############@@@#@@########


var nasal_dirs = nil;
# Dependency resolution horrors:
var already_loaded_resources = [
	"props",
];
# TODO: use LoadRules DSL for this
var guaranteed_resources = [
	"globals", "string", "debug", "io", "bits", "math", "std/string.nas",
];
var make_resources = func() {
	var append_nasal = func(list) {
		forindex (var i; list)
			list[i] = list[i].getValue() ~ "/Nasal";
		return list;
	}
	getvalues = func(list) {
		forindex (var i; list)
			list[i] = list[i].getValue();
		return list;
	}
	nasal_dirs = append_nasal(props.globals.getNode("/sim").getChildren("fg-root"))
	           ~ append_nasal(props.globals.getNode("/sim").getChildren("fg-home"))
	           ~ getvalues(props.globals.getNode("/sim").getChildren("fg-nasal"))
	           ~ [getprop("/sim/aircraft-dir") ~ "/Nasal"];
	var NasalNode = props.globals.getNode("nasal");
	var modules = NasalNode.removeChildren();
	var load_listener = func(n) {
		if (!n.getValue()) return;
		var parent = n.getParent();
		if (parent.getValue("loaded")) {
			# The caller must explicity run this if they want a reload:
			#parent.setValue("unload-requested", 1);
			# Otherwise, we return to avoid duplicate loading:
			return;
		}
		printlog("info", "loading Nasal component at ", parent.getPath());
		var name = parent.getName();
		if (name == "file")
			io.load_nasal(parent.getValue("absolute-path"), 
			              parent.getValue("namespace"));
		else {
			if (name == "nasal") {
				# Load "standard" directories before extra files:
				var resources = parent.getChildren("base-dir")
				               ~parent.getChildren("file");
				# And mandatory before all:
				foreach (var dependency; guaranteed_resources) {
					if (dependency != nil)
						require(dependency);
				}
			} else {
				# Outside-in loading order:
				var resources = parent.getChildren("file")
				               ~parent.getChildren("dir");
			}
			foreach (var resource; resources) {
				if (name == "nasal" and resource.getName() == "file") {
					parent.setValue("loaded", 1); # hack to trigger nasal-dir-initialized
				}
				resource.setValue("load-requested", 1);
			}
		}
		parent.setValue("loaded", 1);
		n.setValue(0);
	}
	var unload_listener = func(n) {
		if (!n.getValue()) return;
		var parent = n.getParent();
		if (!parent.getValue("loaded"))
		{ n.setValue(0); return }
		parent.setValue("loaded", 0);
		settimer(func n.setValue(0), 0); #next frame, after all listeners have run, FIXME
	}
	var get_abs_path = func(node) {
		node.getParent().getValue("absolute-path")
		~"/"~node.getValue("path");
	}
	var get_module_name = func(node) {
		var current = node;
		while (current.getParent().getName() != "base-dir")
			current = current.getParent();
		return split(".", current.getValue("path"))[0];
	}
	var init_module_part = func(node, path=nil) {
		if (path != nil) node.setValue("path", path);
		node.initNode("loaded", 0, "BOOL");
		setlistener(
			node.initNode("load-requested", 0, "BOOL"),
			load_listener);
		setlistener(
			node.initNode("unload-requested", 0, "BOOL"),
			unload_listener);
		if (node.getName() == "nasal") {
			var this = setlistener(
				node.getNode("loaded"),
				func(n) {
					if (!n.getValue()) return;
					removelistener(this);
					var n = props.globals.getNode("sim/signals/nasal-dir-initialized");
					if (n == nil) return;
					n.setValue(1);
					n.remove();
				});
		} elsif (node.getName() == "base-dir") {
			node.setValue("absolute-path", normpath(path));
		} else {
			node.setValue("absolute-path", get_abs_path(node));
			node.setValue("namespace", get_module_name(node));
		}
		return node;
	}
	var make_basedir = func(path) {
		if (path[0] != `/`)
			die("paths must be absolute");
		path = normpath(path);
		var subpaths = directory(path);
		if (subpaths == nil) return; # directory didn't exist
		var dir = init_module_part(NasalNode.addChild("base-dir"), path);
		foreach (var subp; subpaths) {
			make_child(dir, subp);
		}
	}
	var make_child = func(node, path) {
		if (path == "." or path == "..") return;
		var abs_path = node.getValue("absolute-path") ~ "/" ~ path;
		var subpaths = directory(abs_path);
		if (subpaths == nil) {
			if (split(".", path)[-1] == "nas")
				init_module_part(node.addChild("file"), path);
		} elsif (size(subpaths)) {
			var dir = init_module_part(node.addChild("dir"), path);
			foreach (var subp; subpaths) {
				make_child(dir, subp);
			}
		}
	}

	init_module_part(NasalNode);
	foreach (var abspath; nasal_dirs)
		make_basedir(abspath);
	# Backwards compatibility: /nasal/{module}/{file|script}
	foreach (var mod; modules) {
		foreach (var fN; mod.getChildren("file")) {
			var file = NasalNode.addChild("file");
			file.initNode("loaded", 0, "BOOL");
			file.setValue("namespace", mod.getName());
			file.setValue("absolute-path", resolvepath(fN.getValue()));
			setlistener(
				file.initNode("load-requested", 0, "BOOL"),
				load_listener);
			setlistener(
				file.initNode("unload-requested", 0, "BOOL"),
				unload_listener);
		}
		foreach (var scrN; mod.getChildren("script")) {
			var script = NasalNode.addChild("script");
			script.setValue("namespace", mod.getName());
			script.initNode("loaded", 0, "BOOL");
			setlistener(
				script.initNode("load-requested", 0, "BOOL"),
				(func {
					var source = scrN.getValue();
					var path = scrN.getPath();
					var script = script;
					func(n) {
						if (!n.getValue()) return;
						var parent = n.getParent();
						if (parent.getValue("loaded"))
							parent.setValue("unload-requested", 1);
						parent.setValue("loaded", 1);
						n.setValue(0);
						var module = script.getValue("namespace");
						if (!contains(globals, module))
							globals[module] = {};
						elsif (typeof(globals[module]) != "hash")
							die("io.load_nasal(): namespace '" ~ module ~ "' already in use, but not a hash");

						var code = call(func compile(source, path), nil, var err = []);
						if (size(err)) {
							if (substr(err[0], 0, 12) == "Parse error:") { # hack around Nasal feature
								var e = split(" at line ", err[0]);
								if (size(e) == 2)
									err[0] = string.join("", [e[0], "\n  at ", path, ", line ", e[1], "\n "]);
							}
							for (var i = 1; (var c = caller(i)) != nil; i += 1)
								err ~= subvec(c, 2, 2);
							printerror(err);
							return 0;
						}
						call(bind(code, globals), nil, nil, globals[module], err);
						if (size(err)) {
							err[0] = "Error loading aircraft's Nasal module: "~err[0];
							printerror(err);
						}
					};
				})()
			);
			setlistener(
				script.initNode("unload-requested", 0, "BOOL"),
				unload_listener);
		}
	}
	# Finally, some modules have already been loaded:
	foreach (var dependency; already_loaded_resources) {
		if (dependency != nil)
			find_resource(dependency, func(n) n.setValue("loaded", 1));
	}
}

# A few rules on top of ordinary resolving (see http://wiki.flightgear.org/
# Resolving_Paths). Tries prepending Nasal/ and appending .nas.
#
# @param multiple Must be a vector or nil; if vector, any matching paths are
#                 appended to it; if nil, the function returns the first
#                 matching path.
var resolvenasal = func(path, multiple=nil) {
	var p = path;
	var try = resolvepath(path);
	if (try != "")
		if (multiple == nil) return try;
		else append(multiple, try);
	if (split("/", path)[0] != "Nasal") {
		path = "Nasal/"~path;
		var try = resolvepath(path);
		if (try != "")
			if (multiple == nil) return try;
			else append(multiple, try);
	}
	if (split(".", path)[-1] != "nas") {
		path = path~".nas";
		var try = resolvepath(path);
		if (try != "" and multiple != nil)
			append(multiple, try);
		return try;
	}
}

var find_resource = func(path, multiple=nil) {
	if (typeof(multiple) == 'func') {
		var fn = multiple;
		multiple = [];
	} else {
		var fn = nil;
	}
	var p = path;
	if (resolvenasal(path, var path = []) == "")
		die("nasal path could not be resolved: "~p);
	var NasalNode = props.globals.getNode("nasal");
	var startswith = func(a,b) {
		if (typeof(b) == 'vector') {
			foreach (var c; b)
				if (caller(0)[1](a,c))
					return 1;
			return 0;
		} else substr(b,0,size(a)) == a;
	}
	var equals = func(a,b) {
		if (typeof(b) == 'vector') {
			foreach (var c; b)
				if (caller(0)[1](a,c))
					return 1;
			return 0;
		} else a == b;
	}
	var _find_resource = func(node) {
		foreach (var resource; node.getChildren("dir")~node.getChildren("file")) {
			if (equals((var abs = resource.getValue("absolute-path")), path))
				if (multiple == nil) return resource;
				else append(multiple, resource);
			elsif (startswith(abs, path)) {
				var res = _find_resource(resource);
				if (res != nil)
					if (multiple == nil) return res;
					else die();
			}
		}
		nil;
	}
	foreach (var basedir; NasalNode.getChildren("base-dir"))
		if (startswith(basedir.getValue("path"), path)) {
			var res = _find_resource(basedir);
			if (res != nil) 
				if (multiple == nil) return res;
				else append(multiple, res);
		}
	# Search additional added files:
	var res = _find_resource(NasalNode);
	if (multiple != nil and size(multiple)) {
		if (fn != nil)
			foreach (var res; multiple)
				fn(res);
		return;
	} elsif (res != nil) return res;
	else {
		debug.dump(path);
		die("resource not found! please use io.load_nasal instead");
	}
	printlog("info", "Creating new nasal resource for path "~p);
	var resource = NasalNode.addChild("file");
	resource.setValue("path", p);
	resource.setValue("absolute-path", path);
	resource.initNode("loaded", 0, "BOOL");
	setlistener(
		node.initNode("load-requested", 0, "BOOL"),
		func(n) {
			if (!n.getValue()) return;
			var parent = n.getParent();
			if (parent.getValue("loaded"))
				parent.setValue("unload-requested", 1);
			io.load_nasal(parent.getValue("absolute-path"),
			              parent.getValue("namespace"));
			parent.setValue("loaded", 1);
			n.setValue(0);
		});
	node.initNode("unload-requested", 0, "BOOL");
	node.setValue("namespace", split(split(path, "/")[0], ".")[0]);
	return resource;
}

# Init the property tree:
make_resources();
