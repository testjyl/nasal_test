    var help = "
    Run a Nasal test in a near-FG environment.

    Usage:
        nasal run_test.nas FILE [[-c COMMAND|--VARNAME=VALUE] ...] [-- [ARG ...]]

    Arguments:
        -c <command>    Run a string as a function.
        [ARG ...]       Arguments passed to the script itself.

    Other arguments of this form:
        --${varname}=${value}
      are added as local variables to the running
      namespace, replacing dashes ('-') with
      underscores ('_').
    ";

    if (size(arg) < 1) die("need a script to run");
    if (arg[0] == "-h" or arg[0] == "--help") {
        print(substr(help, 1, size(help)-1));
        return 0;
    }

    var orig_symbols = {};

    (func(global_vars) {
        var symbol_list = keys(global_vars);
        foreach (var s; symbol_list) {
            if (s != "help" and s != "arg" and s != "orig_symbols")
                orig_symbols[s] = global_vars[s]; # N.B.: preseves intern'ing or lack thereof :/
        }
    })(closure(caller(0)[1]));

    var globals = {};
    var global_lexical_scope = bind(func, globals);
    (func {
        foreach (var s; keys(orig_symbols)) {
            globals[s] = orig_symbols[s];
        }
    })();
    var clr = caller(0)[0];
    var ns = {}; # clean namespace for script(s)
    var file = arg[0];

    var capture = nil;
    if (size(arg) > 1)
    foreach (var a; arg[1:]) {
        print("****arg : ["~a~"]"~size(a)~(a[0] == 45)~a[1]~"\n");
        if (capture != nil) {
            if (capture == "--")
                append(ns.arg, a);
            else {
                if (capture == "command")
                   call(bind(compile(a, "<command>"), globals), nil, ns);
                capture = nil;
            }
        } 
        elsif (a == "-c") {
            capture = "command"
        }
        elsif (size(a) > 2 and substr(a,0,2)=="--") {
            a = substr(a, 2);
            if ((var f = find("=", a)) >= 0) {
                var tmp = substr(a, 0, f);
                var varname = "";
                for (var i=0; i<size(tmp); i+=1) {
                    var sb = substr(tmp,i,1);
                    if (sb[0] == "-"[0]) varname ~= "_";
                    elsif (sb[0] != "_"[0] and
                        (sb[0] < "a"[0] or sb[0] > "z"[0]) and
                        (sb[0] < "A"[0] or sb[0] > "Z"[0]) and
                        (sb[0] < "0"[0] or sb[0] > "9"[0]))
                        die("bad character in variable name '"~sb~"'");
                    else varname ~= sb;
                }
                var value = substr(a, f+1);
                ns[varname] = value;
            } elsif (a == "") {
                capture = "--";
                ns.arg = [];
            }
            else die("unknown arg: --"~a);
        }
        else die("unknown arg: "~a);
    }

    string = {
        join: func(sep,arg) {
            if (!size(arg)) return sep~"";
            res = "";
            foreach (var s; arg) res ~= s ~ sep;
            return substr(res, 0, size(res)-size(sep))
        },
    };

    load_nasal = func(file, module=nil) {
        # Loads Nasal file into namespace and executes it. The namespace
        # (module name) is taken from the optional second argument
        # default to global namespace
        # (derive from io_load_nasal)
        # Usage:   io.load_nasal(<filename> [, <modulename>]);

        # If !module, fall through to ns above
        var ns = ns;
        if (module) {
            if (!contains(globals, module))
                globals[module] = {};
            elsif (typeof(globals[module]) != "hash")
                die("io.load_nasal(): namespace '" ~ module ~ "' already in use, but not a hash");
            var ns = globals[module];
            #print("info ", "loading ", file, " into namespace ", module,"\n");
        }
        #else print("info ", "loading ", file, " into global namespace\n");
       
        var code = call(func compile(readfile(file), file), nil, var err = []);
        if (processError(err)) return 0;
        call(bind(code, globals, global_lexical_scope), nil, nil, ns, err);
        if (size(err)) printerror(err);
        return !size(err);
    };
    readfile = func(file) {
        if ((var st = io.stat(file)) == nil) die("Cannot stat file: " ~ file);
        var sz = st[7];
        var buf = bits.buf(sz);
        io.read(io.open(file), buf, sz);
        return buf;
    };
    processError = func(err) {
        if (size(err)) {
            if (substr(err[0], 0, 12) == "Parse error:") { # hack around Nasal feature
                var e = split(" at line ", err[0]);
                if (size(e) == 2)
                    err[0] = string.join("", [e[0], "\n  at ", file, ", line ", e[1], "\n "]);
            }
            for (var i = 1; (var c = caller(i)) != nil; i += 1)
                err ~= subvec(c, 2, 2);
            printerror(err);
            return 1;
        }
        return 0;
    }
    printerror = func(err) {
        if (!size(err))
            return;
        print(sprintf("%s:\n at %s, line %d\n", err[0], err[1], err[2]));
        for (var i = 3; i < size(err); i += 2)
            print(sprintf("  called from: %s, line %d\n", err[i], err[i + 1]));
    };

    # copy symbols over (this can be automated too)
    globals.load_nasal = load_nasal;
    globals.readfile = readfile;
    globals.printerror = printerror;
    globals.processError = processError;

    globals.globals = globals;

    load_nasal(file); # run script
