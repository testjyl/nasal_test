
##############################################################################

load_nasal("./mockproputil.nas");
load_nasal("./mockprops.nas","props");
load_nasal(fgdatadir~"Aircraft/777/Models/Instruments/MFD/mfd.nas","b777");
load_nasal("./nasmine.nas","nsm");

nsm.describe("a panel registry should",func() {
    nsm.it("update an element when added",func(){
        var elt = {
                updateCalled: 0,
                update : func(value) {
                    me.value = value;
                    me.updateCalled = 1;
                }
        };
        var prop = "controls/flight/speedbrakeangle";
        setprop(prop,54);
        var panelR = b777.PanelRegistry.new();
        panelR.add(prop,elt);
        panelR.updateAll();
        
        nsm.expect(elt.value).toBe(54);
    });
    nsm.it("not update an element on nil property",func(){
        var elt = {
                value = 0,
                updateCalled: 0,
                update : func(value) {
                    me.value = value;
                    me.updateCalled = 1;
                }
        };
        var prop = "controls/flight/unknown";
        var panelR = b777.PanelRegistry.new();
        panelR.add(prop,elt);
        panelR.updateAll();
        
        nsm.expect(elt.value).toBe(0);
    });

});

print("\n-------done-----------\n");