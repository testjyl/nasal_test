
##############################################################################

load_nasal("./mockproputil.nas");
load_nasal("./mockprops.nas","props");
load_nasal(fgdatadir~"Aircraft/777/Nasal/engine.nas","b777");

setprop("sim/time/elapsed-sec",12);

var leftEngine = b777.Engine.new(0);

setprop("sim/time/elapsed-sec",72);

leftEngine.updateOilTemp();

print("oil temperature is :", getprop("engines/engine[0]/oil-temperature-degc") );
print("\n-------done-----------\n");