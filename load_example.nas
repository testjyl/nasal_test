var globals = caller(0)[0];
var global_lexical_scope = caller(0)[1];

load_nasal = func(file, module=nil) {
	# Loads Nasal file into namespace and executes it. The namespace
	# (module name) is taken from the optional second argument
	# default to global namespace
	# (derive from io_load_nasal)
	# Usage:   io.load_nasal(<filename> [, <modulename>]);
	#
	var ns = globals;
	if (module) {
	    if (!contains(globals, module))
	        globals[module] = {};
	    elsif (typeof(globals[module]) != "hash")
	        die("io.load_nasal(): namespace '" ~ module ~ "' already in use, but not a hash");
	    ns = globals[module];
	    print("info ", "loading ", file, " into namespace ", module,"\n");
	}
	else print("info ", "loading ", file, " into global namespace\n");
	
	var code = call(func compile(readfile(file), file), nil, var err = []);
	processError(err);
	call(bind(code, globals,global_lexical_scope), nil, nil, ns, err);
	if (size(err)) printerror(err);
	return !size(err);
};
readfile = func(file) {
	if ((var st = io.stat(file)) == nil) die("Cannot stat file: " ~ file);
	var sz = st[7];
	var buf = bits.buf(sz);
	io.read(io.open(file), buf, sz);
	return buf;
};
processError = func(err) {
    if (size(err)) {
        if (substr(err[0], 0, 12) == "Parse error:") { # hack around Nasal feature
            var e = split(" at line ", err[0]);
            if (size(e) == 2)
                err[0] = string.join("", [e[0], "\n  at ", file, ", line ", e[1], "\n "]);
        }
        for (var i = 1; (var c = caller(i)) != nil; i += 1)
            err ~= subvec(c, 2, 2);
        printerror(err);
        return 0;
    }
}
printerror = func(err) {
	if (!size(err))
		return;
	print(sprintf("%s:\n at %s, line %d", err[0], err[1], err[2]));
	for (var i = 3; i < size(err); i += 2)
		print(sprintf("  called from: %s, line %d", err[i], err[i + 1]));
};
##############################################################################

var fg_dir = "/home/jylebleu/devmisc/fg/fgfs/install/fgfs/fgdata/";
var local_dir = "/home/jylebleu/devmisc/fg/nasal_tests/";
load_nasal(local_dir~"mockprop.nas");
load_nasal(fg_dir~"Nasal/string.nas");
load_nasal(local_dir~"toto.nas","toto");

toto.hello("yeahhhh");


setprop("/orientation/yaw-deg", 33.5);
print("yaw-deg is:", getprop("/orientation/yaw-deg") );

print("\n-------done-----------\n");