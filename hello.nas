# hello.nas
print("Hello\nWorld!\n");
print("---------------------\n");

var clr = caller(0);
print(typeof(clr[0])," ",clr[0],"\n");
print(typeof(clr[1])," ",clr[1],"\n");
print(typeof(clr[2])," ",clr[2],"\n");
print(typeof(clr[3])," ",clr[3],"\n");
print("---------------------\n");

# a helper function to return the namespace of the caller
var get_current_namespace = func return caller()[0];

# get the current namespace/hash
var ns = get_current_namespace();

# add a new function to the namespace:
ns.hello_world = func print("hello world!");

# now, we have a new function in our current namespace
# so we don't need to prepend "ns" here - we can directly
# call it like this:
    hello_world();

var f = "/home/jylebleu/workspaces/kepler_scala/nasal_tests/toto.nas";
var src = io.readfile(f);
var code = compile(src);
